# HOW TO USE IMAGE RICALIBRATION TOOLBOX

#### SUB PAKCAGE RADOCCT
For specific indication on how to use this package refer to the [official documentation](http://www-personal.acfr.usyd.edu.au/akas9185/AutoCalib/AutoCamDoc/index.html)
* Add the path in which you extracted the RADOCCT toolbox to matlab (with the *Add to path -> selected folder and subfolders*)
* Navigate to the folder in wich you saved your images
NB: the images must have an identical base word (example Image) and some suffix (ex: Image1, Image2...ImageN)
* Run the `calib` command in the Matlab Command Window (be sure to be in the images folder)
* A GUI will open... click on the *Read Images* button and follow the steps.
* If everithing went the right way a Window showing all your images should appear
* Click on *Extract grid corners*, you will be asked to input some parameters:
  * The firs one is the number of the images you want to process, press Enter to choose all
  * The second and third one are the lenght of each squares in millimeters

For one image to be processed it will take circa 10 seconds.
Now just click on the *Calibration* button to be printed the following matrices:
* Focal Length
* Principal point
* Skew
* Distortion
* Pixel error

Finally click the *Save* button to generate the following files:
* Calib_results.m
* Calib_Results.mat 

Note that an object called `Calib_Results.mat ` will be saved in the directory in which you where when clicking the *Calibration* button


#### SUB PAKCAGE RADLOCCT
For specific indication on how to use this package refer to the [official documentation](http://www-personal.acfr.usyd.edu.au/akas9185/AutoCalib/AutoLaserCamDoc/index.html)
##### Requirements
For this toolbox to work you will need the following files:
* An ASCII file that contains the laser scans with the specific format mentioned in the official documentation (<timestamp> StartAngleRads AngleIncrementRads EndAngleRads RangeUnitType NoAngles [Ranges])
* An ASCII file containing the time_stamps of each captured screenshot from the camera
* A `Calib_Results.mat` file which you can generate following the *SUB PAKCAGE RADOCCT* steps
##### Steps
* First you need to execute the `RADLOCCT` command in the matlab terminal (having already added the RADLOCCT folders and subfolders)
* Then you need to move int the folder containing your files (the one mentioned in the *Requirements* part) and click on the *Read Data* button. You will have to 
input the file names.

# HOW TO TAKE RECALIBRATION BAG


First you need to source the *source-all.bash* file in the tradr_wss folder.
Then execute the following command to launch the gui robots
* `roscd tradr_init/scripts/`
* `./gui_robots.sh`

Clik on the *UGV* and *realsense* buttons and wait for them to turn green.
In another shell source the *source-all.bash* file and execute `ros_roma` or `ros_talos` (depending on which robot you want to use). Then execute `rviz` and add the following topics:
* /scan/laserscan
* /camera/rgb/image_rect_color/compressed

Use the chekboard to see when both the camera and the laserscan are able to see it (the laserscan will present a horizontal line). When you are confident about the position of the checkboard, open another shell, connect to the robot via ssh, source the main file in *Work/tradr_wss/* and execute:
* `roscd tradr_init/scripts`
* `./record_bag_recalibration.sh`
* To stop it simply press ctrl+c

A new bag file will appear in the `~/.ros/` direcotry, you must use that bag for the later steps


# HOW TO RECALIBRATE WITH PYTHON
## Requirements
For this steps you will need some python dependencies installed, specifically the ones specified in the `requirements.txt`,
just use `sudo pip install ` for alla of them.
You will also need MATLAB (raccomended version 2107b) and you will have to enable the python engine on it.

### Pyhton engine on Matlab
To use the pyhton engine you can follow [this instruction](https://it.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html).
If you get come sort of error like this `EnvironmentError: Please reinstall MATLAB Engine for Python or contact MathWorks Technical Support for assistance: /usr/lib/x86_64-linux-gnu/libstdc++.so.6: version CXXABI_1.3.8 not found ` you 
need to install the correct gcc and gpp version with the following commands:
* `sudo add-apt-repository ppa:ubuntu-toolchain-r/test`
* `sudo apt-get update`
* `sudo apt-get install gcc-4.9 g++-4.9`

## Parameters to change
You can choose to set some parameters based on the data you are evaluating.
### In play_bag.py
* *IMAGES_NUM*: the number of images you want to grab from the camera video (type int)
* *RANGE_UNIT_TIPE* : the type of the scan (int) [1-mm, 2-cm, 3-m, 4-km], currently working only for type 4

### In pylab.py

* *DX/DY*: the lengh across the x/y-axis of each square of the checkboard
* *INITIAL_TRASLATION_VECT* : Initial translation vector [used here](http://www-personal.acfr.usyd.edu.au/akas9185/AutoCalib/AutoLaserCamDoc/index.html)
* *INITIAL_ROTATION_VECT* : Initial translation vector [used here](http://www-personal.acfr.usyd.edu.au/akas9185/AutoCalib/AutoLaserCamDoc/index.html)
* *DESKTOP* : boolean, set it to True to run matlab with the desktop version (used for debugging)
* *MIN_MAX_AXIS* : A vector of four element that crops the point selection, you may want to use this when dealing with a very 
large datasets and a small checkboard, by changinh this parameter you can better focus the bound clicking (explained int the following
section). Note that this parameter is strictly tied with the RANGE_UNIT_TYPE
* ~~*ROUGH_LINE_TH*: Rough thresholding for extracting lines~~
* ~~*FINE_LINE_TH*: Fine thresholding for extracting lines~~

## How to use
To use this just type the following command:
* `python play_bag path_to_bag`

During the execution various images will be shown:
* First you will se the captured screenshots form the camera
* Then you will be promped with a window in which you have to determine the checkboard bounds, to do so 
just follow this steps:
    1. First find in the laser scan (the figure on the right) the bonds corrisponding to the checkboard on the left image
    2. Move the cursor where you belive the first bound is and then click
    3. Repeat step 2 for the second bound
    4. You can choose to exit the bound selection by pressing 'e' or to skip an image by pressing 'enter'
* Finally the results will be saved in the *RESULTS_DIR*, both in .mat and .csv form