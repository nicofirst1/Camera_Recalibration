import sys
from PIL import Image
import cv2
import os
import rosbag
from cv_bridge import CvBridge
from pylab import *
from os import listdir
from os.path import isfile, join
import scipy.io
import pandas as pd
import numpy as np


# global variables
USAGE = "Usage: play_bag path_to_bag"
VERBOSE = False
IMAGES_NUM = 10  # number of screenshot of the ralsens camera video
IMAGE_DIR = "Images/"
DATA_DIR = "Data/"
REULTS_DIR="Results/"
# RANGE_CUTOFF=2.5
# RangeUnitType
#
# = 1 for mm
#
# = 2 for cm
#
# = 3 for m
#
# = 4 for km

RANGE_UNIT_TIPE = 3


def process_images(bag, initial_time, sleep):
    """Function to read images from a rosbag object and saving them with timestamp
    @:param bag: the bag object
    @:type: rosbag.Bag()
    @:param initial_time: the start time of the bag in seconds
    @:type: int
    @:param sleep: the interval of seconds in which capture the image
    @:type: int"""

    if VERBOSE: print(bag, initial_time, sleep)

    images = []  # list of tuples where elem[0] is the data and elem[1] is the timestamp
    # read messages from bag
    for topic, msg, t in bag.read_messages("/camera/rgb/image_rect_color/compressed"):
        # if the timestamp is a multiple of sleep then add the data to images
        # there will be multiple images from the same second in the timestam due to the camera Hz,
        # that is what the second condition is for
        if (t.secs - initial_time) % sleep == 0 and t.secs not in [elem[1].secs for elem in images]:
            images.append((msg, t))

    # converting images and getting time staps list
    images, time_stamp = data2image(images)

    # saving images
    print("Saving " + str(len(images)) + " images in " + IMAGE_DIR + "...")
    idx = 1
    for image in images:
        pi = Image.fromarray(image)
        name = IMAGE_DIR + "img" + str(idx) + ".jpg"
        pi.save(name)
        if VERBOSE: print(name + " saved")
        idx += 1

    # saving timestamps in file
    for time in time_stamp:
        with open(DATA_DIR + "images_time_stamps.txt", "a+") as file:
            file.write(str(time) + "\n")


def process_laser_scan(bag):
    """
    Generate the string to put into the matlabLaserHorizontalData file:
    <timestamp> StartAngleRads AngleIncrementRads EndAngleRads RangeUnitType NoAngles [Ranges]
    @:param bag: the rosbag object
    """
    print("Generating Laser Scan file...")
    laser_str = ""

    for topic, msg, t in bag.read_messages("/scan"):
        laser_str += str(msg.header.stamp.to_sec())
        laser_str += " %0.3f" % msg.angle_min + " %0.3f" % msg.angle_increment + \
                     " %0.3f" % msg.angle_max + " "+ str(RANGE_UNIT_TIPE)+" " + str(len(msg.ranges))
        for range_val in msg.ranges:
            # if range_val > RANGE_CUTOFF:
            #     laser_str += " nan"
            # else:
            laser_str += " %0.3f" % range_val
        laser_str += "\n"
    # print(laser_str)
    with open(DATA_DIR + "data.txt", "w+") as file:
        file.writelines(laser_str)


def clean_env(img_dir, data_dir, results_dir):
    """Function to delete the content of the Data and Image direcotries
    @:param img_dir: the image direcotory
    @:type: str
    @:param data_dir: the data direcoty
    @:type: str"""

    # list all the files in the direcotries
    img_files = [img_dir + f for f in listdir(img_dir) if isfile(join(img_dir, f))]
    data_files = [data_dir + f for f in listdir(data_dir) if isfile(join(data_dir, f))]
    results_dir = [results_dir + f for f in listdir(results_dir) if isfile(join(results_dir, f))]

    # exclude the .gitignore
    img_files = [elem for elem in img_files if ".gitignore" not in elem]
    data_files = [elem for elem in data_files if ".gitignore" not in elem]
    results_dir = [elem for elem in results_dir if ".gitignore" not in elem]

    #removing files
    map(lambda elem: os.remove(elem),img_files)
    map(lambda elem: os.remove(elem),data_files)
    map(lambda elem: os.remove(elem),results_dir)


def data2image(data_images):
    """Function to convert  sensor_msgs/CompressedImage  to cv images
    @:param data_images: list of tuples, where the first element is the data form the compressed image
    and the secont is a ros.Time object
    @:return : returns two lists:
        images: list of images in cv2 format
        time_stamps: list of timestamps associated to the images"""

    images = []
    time_stamps = []
    # convert data to image
    for elem in data_images:
        cv_image = CvBridge().compressed_imgmsg_to_cv2(elem[0], "bgr8")
        images.append(cv_image)
        time_stamps.append(elem[0].header.stamp.to_sec())

    if VERBOSE:  # show images
        for elem in images:
            cv2.imshow('image', elem[0])
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    return images, time_stamps

def mat2csv(file):
    """Convert a matlab .mat file into a .csv
    @:param file: the relative path to the file
    @:type: str"""
    file_name=os.path.splitext(file)[0]
    mat = scipy.io.loadmat(file)
    for i in mat:
        if '__' not in i and 'readme' not in i:
            np.savetxt(("file.csv"), mat[i], delimiter=',')


if __name__ == '__main__':
    # get the argumetns from the command line
    args = sys.argv
    if len(args) != 2:
        print("Invalid arguments!\n" + USAGE)
        sys.exit(1)

    # taking bag path and opening it
    bag_file = args[1]
    bag = rosbag.Bag(bag_file)

    print("Cleaning enviroment...")
    clean_env(IMAGE_DIR, DATA_DIR, REULTS_DIR)

    print("Bag correctly opened")
    if VERBOSE:
        print(bag)

    # getting bag times and determing sleep duration for taking screenshots
    bag_duration = bag.get_end_time() - bag.get_start_time()
    time_to_sleep = bag_duration / IMAGES_NUM

    # get the lists of images and time_stamps in which they were taken
    process_images(bag, int(bag.get_start_time()), int(time_to_sleep))
    process_laser_scan(bag)
    bag.close()

    # running intrinsic calibration
    eng = run_engine()
    RADOCCT(eng, IMAGE_DIR)

    # moving datas into Data dir
    os.rename(IMAGE_DIR+"calib_data.mat", DATA_DIR+"calib_data.mat")
    os.rename(IMAGE_DIR+"Calib_Results.mat", DATA_DIR+"Calib_Results.mat")
    os.rename(IMAGE_DIR+"Calib_Results.m", DATA_DIR+"Calib_Results.m")

    #running RADLOCCT toolbox
    RADLOCCT(eng, DATA_DIR)

    # moving results into Result dir
    os.rename(DATA_DIR+"LaserScans.mat", REULTS_DIR+"LaserScans.mat")
    os.rename(DATA_DIR+"CalibPoints.mat", REULTS_DIR+"CalibPoints.mat")
    os.rename(DATA_DIR+"ManClstrs.mat", REULTS_DIR+"ManClstrs.mat")

    # print("Converting .mat to .csv...")
    # #converting .mat to .csv
    # mat2csv(REULTS_DIR+"LaserScans.mat")
    # mat2csv(REULTS_DIR+"CalibPoints.mat")
    # mat2csv(REULTS_DIR+"ManClstrs.mat")
    #
