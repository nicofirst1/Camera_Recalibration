import matlab.engine

######RADOCCT#########
# misures across the X|Y axis of the checkboard squares in millimeters
from play_bag import RANGE_UNIT_TIPE

######RADOCCT#########
DX = 40
DY = 40

######RADLOCCT#########
global MIN_MAX_AXIS
ROUGH_LINE_TH = 0.5
FINE_LINE_TH = 0.2
INITIAL_TRASLATION_VECT = matlab.double([2, 0, 0])
INITIAL_ROTATION_VECT = matlab.double([0, 0, 0])
MIN_MAX_AXIS = [-1.5, 1.5, 0, 1]  # [minX, maxX, minY,maxY], tied to RANGE_UNIT_TIPE
DESKTOP = False


def run_engine():
    """Start matlab engine and add path to toolboxes
    @:return: the matlab engine"""
    print("Starting matlab engine...")
    # if you want to open up a matlab window while the code execute (for debug reasons use the -desktop option in the
    # matlab engine.start_matlab, be aware that the window will close as soon as the program stops its exeution,

    #  you may want to use some raw_input to stop the process
    if DESKTOP:
        engine = matlab.engine.start_matlab("-desktop")
    else:
        engine = matlab.engine.start_matlab()
    print("Matlab engine is running!")
    # adding main folders
    engine.addpath("RADOCCToolbox")
    engine.addpath("RADLOCCToolbox")

    # matlab addpath is not recursive so we need to manually add all the subfolders
    engine.addpath("RADOCCToolbox/CornerFinder")
    engine.addpath("RADOCCToolbox/doc")
    engine.addpath("RADLOCCToolbox/doc")
    engine.addpath("RADLOCCToolbox/Functions")
    return engine


def RADOCCT(engine, image_dir):
    """Run the RADOCCT toolbox for intrinsic calibration
    @:param engine: the matlab engine
    @:type: matlab.engine
    @:param image_dir: the directory in which the images are located
    @:type: string"""
    # move to image dir
    engine.cd(image_dir)
    # load images
    engine.ima_read_calib(nargout=0)
    print(engine.workspace)
    # if you want to see the images before they are used uncomment the following line
    # raw_input("Showing selected images\nPress Enter to continue...")

    # extract grid corners with specified DX, DY
    engine.workspace['dX'] = DX
    engine.workspace['dY'] = DY
    engine.click_calib(nargout=0)
    # print(engine.workspace)

    # perform intrinsic calibration
    engine.go_calib_optim(nargout=0)
    # print(engine.workspace)
    engine.saving_calib(nargout=0)
    # moving out of image directory
    engine.cd("../")


def RADLOCCT(engine, data_dir):
    """Run the RADLOCCT toolbox for extrinsic calibration
       @:param engine: the matlab engine
       @:type: matlab.engine
       @:param data_dir: the directory in which the data is located
       @:type: string"""
    global MIN_MAX_AXIS
    # move into the data directory
    engine.cd(data_dir)
    # change workspace variable values in matlab (need conversion into matlab int64->double)
    engine.workspace['roughthin'] = ROUGH_LINE_TH
    engine.workspace['finethin'] = FINE_LINE_TH
    # read datas from Data_dir
    engine.read_data_cb(nargout=0)
    # raw_input("Showing selected images\nPress Enter to continue...")
    # setting automatic estimation
    # if AUTOMATIC_INITIAL_ESTIMATION: engine.workspace['autoinitest']='y'
    # else:
    #     #if no automatic estimation is set, the user must pass a vector of three elemnts
    #     engine.workspace['autoinitest']='n'
    if RANGE_UNIT_TIPE == 1:
        MIN_MAX_AXIS = [elem / 100 for elem in MIN_MAX_AXIS]
    elif RANGE_UNIT_TIPE == 2:
        MIN_MAX_AXIS = [elem / 10 for elem in MIN_MAX_AXIS]
    elif RANGE_UNIT_TIPE == 4:
        MIN_MAX_AXIS = [elem * 100 for elem in MIN_MAX_AXIS]
    engine.workspace['min_max_axis'] = matlab.double(MIN_MAX_AXIS)
    engine.workspace['deltaestin'] = INITIAL_TRASLATION_VECT
    engine.workspace['phiestin'] = INITIAL_ROTATION_VECT
    raw_input("Showing selected images\nPress Enter to continue...")

    # perform manual estimation of checkboard position
    engine.manual_select_cb(nargout=0)
    # calibrate
    engine.calibrate_cb(nargout=0)
    # save results
    engine.save_cb(nargout=0)
    # moving out of image directory
    engine.cd("../")


if __name__ == '__main__':
    eng = run_engine()
    RADLOCCT(eng, "Data/")
